#!/usr/bin/env ruby
require 'set'

def gem?(str)
  regex = /(.*)\s\((\d*\.)*\d*\)/
  str =~ regex ? true : false
end

in_file = ARGF.readlines
repositories = []

in_file.each do |line|
  line = line.split("\t")
  repositories << line
end

out_file = File.new('result.txt', 'w')
total_gems = SortedSet.new

repositories.each do |repo|
  url = repo[0].strip
  branch = repo[1].strip
  gemfile = `git archive --remote=#{url} #{branch} Gemfile.lock`
  gemfile = gemfile.to_s.split("\n")

  gemfile.each do |gem|
    next unless gem?(gem)
    gem = gem.strip.delete('()').sub(' ', "\t")
    total_gems.add(gem)
    out_file.puts(gem)
    puts gem
  end
end

out_file.close
